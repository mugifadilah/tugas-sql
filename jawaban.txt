<!-- Jawaban No 1 Membuat Database -->
CREATE DATABASE myshop;

<!-- Jawaban No 2 Membuat Table di Dalam Database -->
USE myshop;

CREATE TABLE users (
    id int AUTO_INCREMENT PRIMARY KEY ,
    name varchar(255),
    email varchar(255),
    password varchar(255)
);

CREATE TABLE categories (
    id int AUTO_INCREMENT PRIMARY KEY ,
    name varchar(255)
);

CREATE TABLE items (
    id int AUTO_INCREMENT PRIMARY KEY ,
    name varchar(255),
    description varchar(255),
    price int,
    stock int,
    category_id int,
    FOREIGN KEY (category_id) REFERENCES categories(id)
);

<!-- Jawaban No 3 Memasukkan Data pada Table -->
1.) Users
INSERT INTO users(id, name, email, password)
VALUES (1, "John Doe", "john@doe.com", "john123");

INSERT INTO users(id, name, email, password)
VALUES (2, "Jane Doe", "jane@doe.com", "jenita123");

2.) Items
INSERT INTO items(id, name, description, price, stock, category_id)
VALUES (1, "Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1);

INSERT INTO items(id, name, description, price, stock, category_id)
VALUES (2, "Uniklooh", "baju keren dari brand ternama", 500000, 50, 2);

INSERT INTO items(id, name, description, price, stock, category_id)
VALUES (3, "IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);

3.) Categories
INSERT INTO categories(id, name) VALUES (1, "gadget");
INSERT INTO categories(id, name) VALUES (2, "cloth");
INSERT INTO categories(id, name) VALUES (3, "men");
INSERT INTO categories(id, name) VALUES (4, "women");
INSERT INTO categories(id, name) VALUES (5, "branded");

<!-- Jawaban No 4 Mengambil Data dari Database -->
a.) SELECT id, name, email FROM users;
	+------+----------+--------------+
	| id   | name     | email        |
	+------+----------+--------------+
	|    1 | John Doe | john@doe.com |
	|    2 | Jane Doe | jane@doe.com |
	+------+----------+--------------+

b.) SELECT * FROM items WHERE price >= 1000000;
	SELECT * FROM items WHERE name LIKE '%watch%';

c.) SELECT items.id, items.name, items.description, items.price, items.stock, items.category_id, categories.name FROM items INNER JOIN categories ON items.category_id = categories.id;

<!-- Jawaban No 5 Mengubah Data dari Database -->
UPDATE items SET price = 2500000 WHERE name LIKE '%sumsang%';

